=== WP Conversion Booster ===
Tags: notifications, visitors, logging
Requires at least: 4.0
Tested up to: 4.7
Stable tag: 4.3
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

This plugin logs visitor data and displays it as toasts on any page selected.

== Installation ==

1. Upload the plugin files to the `/wp-content/plugins/wp-conversion-booster` directory, or install the plugin through the WordPress plugins screen directly.
2. Activate the plugin through the 'Plugins' screen in WordPress
3. Use the Settings->WCB Settings screen to configure the plugin

== Changelog ==
= 3.5.7.8 = 
* Disabled mobile notifications on plugin install
* Changed $log_array = [] to $log_array = array() in WPCB_Ajax Class. No idea why this was causing a parser error

= 3.5.7.7 = 
* minor bug fixes
= 3.5.7.6 = 
* minor bug fixes

= 3.5.7.5 = 
* minor bug fixes

= 3.5.7.4 = 
* More fixes to activation engine
* Other minor bug fixes

= 3.5.7.3 = 
* Under the hood fixes to activation engine
* Other minor tweaks

= 3.5.7.2 = 
* Upgrade to the activation engine
* Tweaks to the notification intervals

= 3.5.7.1 = 
* Minor fixes

= 3.5.7 = 
* Fix session_start() conflict with other plugins
* Ability to add custom funnels including message pages and the message to display
* Fix error message on upload plugin page

= 3.5.6 = 
* Minor under the hood changes!

= 3.5.5 = 
* Fix for logs disappearing on update. Moved to a more permanent location under wp-content

= 3.5.4 = 
* Added activation tab for licensing

= 3.5.3 = 
* Added watermark for licensing

= 3.5.2 = 
* Remove Advanced Tab As its still in Beta stage

= 3.5.1 = 
* Fix for WooCommerce tab not working

= 3.5 =
* JavaScript: 
    Fix for toasts displaying with nothing in them.
* Features: 
    Custom Messages for different pages (Beta)
    Automatic update check

= 3.4 =
* Plugin Settings
    Name change under Settings Menu
    Updated Plugin Name
    Tabulation of settings into - General, Mobile, WooCommerce and Advanced for clearer understanding
* JavaScript: 
    Resolved issue with toasts being too fast
    Resolved issue with mobile settings not working
* Bugs:
    Resolved issue with plugin error on activation
= 3.0 =
* New Features
    Added options for mobile settings
    Added option for restricting number of days which it shows for

= 1.0 =
* Features
   Added a settings page with several options for customization

= 0.5 =
* Beta Release.

== Upgrade Notice ==
= 3.5.7.8 = 
* Fix for mobile notification settings not being adhered to. User will have to enable the option on plugin install
* Fix for AJAX error in getting funnel logs. 

= 3.5.7.7 = 
* minor bug fixes to CURL error in plugin verification

= 3.5.7.6 = 
* minor bug fixes

= 3.5.7.5 = 
* minor bug fixes

= 3.5.7.4 = 
* More fixes to activation engine
* Other minor bug fixes

= 3.5.7.3 = 
* Under the hood fixes to activation engine
* Other minor tweaks

= 3.5.7.2 = 
* Upgrade to the activation engine
* Tweaks to the notification intervals

= 3.5.7.1 = 
* Minor fixes

= 3.5.7 = 
* Fix session_start() conflict with other plugins
* Ability to add custom funnels including message pages and the message to display
* Fix error message on upload plugin page

= 3.5.6 = 
* Minor under the hood changes!

= 3.5.5 = 
* Fix for logs disappearing on update. Moved to a more permanent location under wp-content

= 3.5.4 = 
* Added activation tab for licensing

= 3.5.3 = 
* Added watermark for licensing

= 3.5.2 = 
* Remove Advanced Tab As its still in Beta

= 3.5.1 =
Upgrade to fix WooCommerce issue.

= 3.5 =
Upgrade to fix issue with display of empty toasts. New beta features added

= 3.4 =
Upgrade for new options tab and new features added.