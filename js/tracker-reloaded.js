jQuery(document).ready(function ($) {
    function getTrackingCookie(name) {
        if (name) {
            if (typeof Cookies.get(name) == "undefined") {
                return true;
            }
        }
        return false;
    }
    if (getTrackingCookie(wpcb.cookieName)) {
        $.ajax({
            type: "GET",
            url: wpcb.ajax_url,
            data: {
                "action": "wpcb_tracker",
                "funnel": wpcb.funnel,
                "type": "track_visit"
            },
            success: function (response) {
                Cookies.set(wpcb.cookieName, "Visited:" + new Date().toJSON().slice(0, 10).replace(/-/g, '/'), {
                    expires: 2
                });
                console.log(response);
            }
        });
    }

});