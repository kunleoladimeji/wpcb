# WordPress Conversion Booster

A WordPress plugin for displaying toast notifications

=== WP Conversion Booster ===
Tags: notifications, visitors, logging
Requires at least: 4.0
Tested up to: 4.7
Stable tag: 4.3
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

This plugin logs visitor data and displays it as toasts on any page selected.

== Installation ==

1. Upload the plugin files to the `/wp-content/plugins/wp-conversion-booster` directory, or install the plugin through the WordPress plugins screen directly.
2. Activate the plugin through the 'Plugins' screen in WordPress
3. Use the Settings->WCB Settings screen to configure the plugin 

== Changelog ==

= 3.5 =
* JavaScript: 
    Fix for toasts displaying with nothing in them.
* Features: 
    Custom Messages for different pages (Beta)
    Automatic update check

= 3.4 =
* Plugin Settings
    Name change under Settings Menu
    Updated Plugin Name
    Tabulation of settings into - General, Mobile, WooCommerce and Advanced for clearer understanding
* JavaScript: 
    Resolved issue with toasts being too fast
    Resolved issue with mobile settings not working
* Bugs:
    Resolved issue with plugin error on activation
= 3.0 =
* New Features
    Added options for mobile settings
    Added option for restricting number of days which it shows for

= 1.0 =
* Features
   Added a settings page with several options for customization

= 0.5 =
* Beta Release.

== Upgrade Notice ==

= 3.5 =
Upgrade to fix issue with display of empty toasts. New beta features added

= 3.4 =
Upgrade for new options tab and new features added.