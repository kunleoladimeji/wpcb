<?php
if (!defined("ABSPATH")) {
    exit;
}
/*
Plugin Name: WP Conversion Booster
Plugin URI: http://myfunnelteam.com/projects/wp-conversion-booster
Description: Increases Conversion Rates On Wordpress Sites
Version: 3.5.7.8
Author: MyFunnelTeam
Author URI: http://myfunnelteam.com
*/


class WC_Order_Tracker
{
    public function __construct()
    {
        
        require plugin_dir_path(__FILE__)."addons/plugin-update-checker-4.0.3/plugin-update-checker.php";
        
        define("WPCB_LOG_DIR", WP_CONTENT_DIR. '/ct-logs/');
        
        include "includes/class-combined-tracker-ajax.php";
        
        if (is_admin()) {
            include "includes/class-combined-tracker.php";
            add_action("plugins_loaded", array($this, "start_session"));
            add_action( "admin_notices", array ($this, "check_activation_status") );
        }
        
        
        
        add_action( "wp_enqueue_scripts", array($this, "reloaded_tracker") );

        //Update checker
        $myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
        'https://bitbucket.org/kunleoladimeji/wpcb',
        __FILE__,
        'wp-conversion-booster'
        );
        $myUpdateChecker->setBranch('wp');
        
        register_activation_hook(__FILE__, array($this, "plugin_activate"));
    }
    public function plugin_activate()
    {
        add_option("ct_activation_status", "", "", true);
        if (!file_exists(WP_CONTENT_DIR. "/ct-logs")) {
            mkdir(WP_CONTENT_DIR. '/ct-logs');
        }
    }

    public function check_activation_status()
    {
        $activation_key = get_option("ct_activation_key", "");
        $activation_status = get_option( "ct_activation_status", false );
        $start = microtime(true);
        $notice_css_class = "notice is-dismissible notice-";
        $message = "";
        if (!$activation_status) {
            $notice_css_class .= "error";
            $message .= "Upgrade WP Conversion Booster To Pro Version & Access All Conversion Boosting Features!";
            printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $notice_css_class ), esc_html( $message ) );
        } else {
            if ($activation_key=="") {
                //Remove the activation status immediately!
                update_option( "ct_activation_status", false, true );
                $notice_css_class .= "error";
                $message .= "Upgrade WP Conversion Booster To Pro Version & Access All Conversion Boosting Features!";
                printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $notice_css_class ), esc_html( $message ) );
            } else {
                $args = array ('method' => 'POST', 'timeout' => 5, "body" => array ("action" => "wpcb_ajax", "requestType" => "checkKey", "key" => $activation_key));
                $response = wp_safe_remote_request( "http://wpconversionbooster.com/wp-admin/admin-ajax.php", $args );
                if (is_array($response)) {
                    $status_message = json_decode( $response["body"] );
                    //Is response an error?
                    if ($status_message->status == "error") {
                        //Remove activated status immediately!
                        update_option( "ct_activation_status", false, true );
                        $notice_css_class .= "error";
                        $message .= "Upgrade WP Conversion Booster To Pro Version & Access All Conversion Boosting Features!";
                        printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $notice_css_class ), esc_html( $message ) );
                    } elseif ($response instanceof WP_Error) {
                        $notice_css_class .= "error";
                        $message .= "There was an error validating your license key. Please contact the plugin developers!";
                        printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $notice_css_class ), esc_html( $message ));
                    }
                }
            }
        }

        return;
    }

    
    public function start_session()
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
    }

    public function show_activation_notice()
    {
        $class = 'notice is-dismissible notice-'. $this->activation_notice["notice_class"];
        ;
        $message = __($this->activation_notice["message"], 'wpcb' );

        printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), esc_html( $message ) );
    }
    
    

    public function reloaded_tracker()
    {
        if (get_option("ct_enabled") != "yes") {
            return;
        }
        $funnel_data = get_option( "ct_custom_funnel", null );
        if ($funnel_data) {
            $funnel_data_array = json_decode( $funnel_data, true );
            if (is_array($funnel_data_array)) {
                $index = 0;
                foreach ($funnel_data_array["item"] as $funnels) {
                    foreach ($funnels as $key => $value) {
                        if ($key == "goal") {
                            if (is_page( $value )) {
                                wp_enqueue_script( "js.cookie", plugin_dir_url( __FILE__ ). "js/js.cookie/js.cookie.js");
                                wp_enqueue_script("tracker", plugin_dir_url(__FILE__). "js/tracker-reloaded.min.js", array("jquery"), 1.0, true);
                                wp_localize_script( "tracker", "wpcb", array("cookieName" => "visited", "funnel"=> $index, "ajax_url" => admin_url( "admin-ajax.php")));
                                return true;
                            }
                        } elseif ($key =="message_pages") {
                            if (is_page( $value )) {
                                //Load notification scripts
                                $activation_status = get_option( "ct_activation_status" );
                                $activation_status = ($activation_status != "") ? "no" : "yes";
                                $mobile_check = get_option( "ct_mobile_enable", "no" );
                                
                                if ($mobile_check == "no") {
                                    $mobile_array["enabled"] = "no";
                                } else {
                                    $mobile_array["enabled"] = "yes";
                                    $mobile_array["position"] = get_option( "ct_mobile_position", "toast-top-center" );
                                }
                                wp_enqueue_style( "toastr", plugin_dir_url( __FILE__ ). "js/toastr/toastr.min.css" );
                                wp_enqueue_script( "toastr", plugin_dir_url( __FILE__ ). "js/toastr/toastr.min.js", array ("jquery") );
                                wp_enqueue_script( "timeago", plugin_dir_url( __FILE__ ). "js/timeago/jquery.timeago.js", array ("jquery") );
                                wp_enqueue_script("notifications", plugin_dir_url(__FILE__)."js/notifications-reloaded.min.js?v=".time(), array("jquery"), 1.2, true);
                                wp_localize_script( "notifications", "wpcb", array ("funnel" => $index, "message" => $funnel_data_array["item"][$index]["message"], "ajax_url" => admin_url( "admin-ajax.php"), "watermarked" => $activation_status, "track_for" => get_option("ct_tracker_days", 4), "mobile" => $mobile_array, "desktop_pos" => get_option("ct_tracker_notifications_position", "toast-bottom-right"), "timeout"=> get_option( "ct_notifications_timeout", 10 )));
                                return true;
                            }
                        }
                    }
                    $index++;
                }
            }
        }
        return false;
    }
}
new WC_Order_Tracker();
