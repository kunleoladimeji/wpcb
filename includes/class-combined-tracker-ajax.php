<?php
if (!defined("ABSPATH")) {
    exit;
}
/**
 * WC_Order_Tracker_AJAX Class
 */
class WC_Order_Tracker_AJAX
{
    public function __construct()
    {
        include "ip2locationlite.class.php";
        
        add_action( "wp_ajax_wpcb_tracker", array ($this, "wpcb_ajax") );
        add_action("wp_ajax_nopriv_wpcb_tracker", array($this, "wpcb_ajax"));
    }

    public function wpcb_ajax()
    {
        if (isset($_REQUEST["type"])) {
            $action = $_REQUEST["type"];
            if ($action=="track_visit") {
                $this->store_log_data("funnel-" .$_REQUEST["funnel"], $_SERVER["REMOTE_ADDR"]) or die("Could not store log data");
            } elseif ($action == "get_logs") {
                $log = $this->retrieve_log_data($_REQUEST["days"], "funnel-".$_REQUEST["funnel"]) or die(json_encode( array ("status" => "error"), JSON_PRETTY_PRINT ));
                //$log["status"] = "success";
                header('Content-type: application/json');
                echo json_encode( $log, JSON_PRETTY_PRINT);
            }
        }
        die();
    }


    public function get_orders()
    {
        $log = array();
        $args = array(
                "post_status" => "any",
                "post_type" => "shop_order",
                "posts_per_page" => -1,
                "order" => "ASC"
            );
        $query = new WP_Query($args);
        $count = 0;
        while ($query->have_posts()) {
            $query->the_post();
            $order = new WC_Order($query->post->ID);
            $log[$count]["name"] = $order->billing_first_name;
            $log[$count]["date"] = $order->order_date;
            $log[$count]["city"] = $order->billing_city;
            $log[$count]["country"] = $order->billing_country;
            foreach ($order->get_items() as $item) {
                $log[$count]["productName"] = $item["name"];
            }
            $count++;
        }
        wp_reset_postdata();
        if (!empty($log)) {
            return $log;
        }
        return false;
    }

    public function store_log_data($funnelID, $ip)
    {
        if ($funnelID && $ip) {
            $time = new DateTime("now");
            $iplite = new ip2location_lite();
            $iplite->setKey("4fc168f6017c0fc8fdee2142673d69aaa625260bec6bab276039588048ed0d0c");
            $file_path = WPCB_LOG_DIR . '/'.$funnelID. '.log';

            $f_handle = fopen($file_path, "a+") or die("Could not write to file '". $file_path. "', please change the folder permission to allow write access");
            $geo_data = $iplite->getCity($ip);
            $log_data = "\r\n" .$time->format('c'). "|" .$geo_data["cityName"]. "|" .$geo_data["regionName"];
            fwrite($f_handle, $log_data);
            fclose($f_handle);
            echo "Thanks for your visit!";
            return true;
        }
        return false;
    }
    
    public function retrieve_log_data($periodFrom, $funnelID)
    {
        $periodFrom = ($periodFrom) ? date_sub(new DateTime(), date_interval_create_from_date_string($periodFrom. " days")) : date_sub(new DateTime(), date_interval_create_from_date_string("4 days"));
        $file_path = WPCB_LOG_DIR . '/'. $funnelID. '.log';
        $log_array = array();
        if (file_exists($file_path)) {
            $f_handle = fopen($file_path, "r") or die("Could not read from log file, it might not have been created");
            $f_logs = fread($f_handle, filesize($file_path));
            $f_logs = explode("\r\n", trim($f_logs));
            if ($f_logs[0] != "") {
                foreach ($f_logs as $key => $value) {
                    $log_split = explode("|", trim($value));
                    if ($periodFrom < new DateTime($log_split[0])) {
                        //Can dump raw log split array, but for easier JS, will give each one keys
                        $log_array[$key] = array("date" => $log_split[0], "city" => $log_split[1], "region" => $log_split[2]);
                    }
                }
                if (!empty($log_array)) {
                    return $log_array;
                }
            }
        }

        return false;
    }
}
new WC_Order_Tracker_AJAX();
