<?php
if (!defined("ABSPATH")) {
    exit;
}
/**
 * Setup and create options page, store option values
 */
class Combined_Tracker_Class
{
    private $options;
    public function __construct()
    {
        include "class-post-walker.php";
        add_action("admin_init", array($this, "register_ct_settings"));
        add_action("admin_menu", array($this, "ct_menu"));
    }
    public function ct_menu()
    {
        add_menu_page(__("Combined Tracker Settings", "ct"), __("WPCB", "ct"), "manage_options", "wcb-options-admin", array($this, "ct_admin_form"), '', 2);
    }

    public function plugin_scripts()
    {
        wp_enqueue_style("magnific-popup", "//cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css");
        wp_enqueue_script("magnific-popup", "//cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js", array('jquery'));
        wp_enqueue_style("chosen", "//cdnjs.cloudflare.com/ajax/libs/chosen/1.6.2/chosen.min.css");
        wp_enqueue_script("angular", "//cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.1/angular.min.js");
        wp_enqueue_script( "bootstrap", "//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js");
        wp_enqueue_style( "bootstrap", "//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" );
        wp_enqueue_script("chosen", "//cdnjs.cloudflare.com/ajax/libs/chosen/1.6.2/chosen.jquery.min.js", array('jquery'));
        wp_enqueue_script( "angular-chosen", "//cdnjs.cloudflare.com/ajax/libs/angular-chosen-localytics/1.6.0/angular-chosen.js", array ('angular'));
    }

    public function checkActivationKey($input)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://wpconversionbooster.com/wp-admin/admin-ajax.php");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "action=wpcb_ajax&requestType=validateKey&referrer=" .$_SERVER["HTTP_HOST"]. "&key=" .trim($input));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec ($ch);
        curl_close ($ch);
        $server_output = json_decode($server_output);
       
        if ($server_output->status != "error") {
            update_option("ct_activation_status", "active");
            $_SESSION["activated"] = "yes";
        } else {
            $_SESSION["activated"] = "no";
        }
        remove_action( "admin_notices", "show_activation_notice" );
        return $input;
    }

    public function register_ct_settings()
    {
        register_setting("ct_options_general", "ct_enabled");
        register_setting("ct_options_general", "ct_tracker_notifications_position");
        register_setting("ct_options_general", "ct_notifications_timeout", "intval");
        register_setting("ct_options_mobile", "ct_mobile_enable");
        register_setting("ct_options_mobile", "ct_mobile_position");
        register_setting("ct_options_woocommerce", "ct_chk_woocommerce");
        register_setting("ct_options_woocommerce", "ct_wc_tracker_message");
        register_setting("ct_options_advanced", "ct_tracker_days", "intval");
        register_setting("ct_options_advanced", "ct_custom_funnel");
        register_setting("ct_options_activation", "ct_activation_key", array($this, "checkActivationKey"));
        if (isset ($_GET["page"]) && $_GET["page"] == "wcb-options-admin") {
            add_action("admin_enqueue_scripts", array($this, "plugin_scripts"));
        }
    }


    public function ct_admin_form()
    {
        ?>        
        <div class="wrap">
        <style>
 body {
     background-color: #F1F1F1;
 }
 .white-popup {
  position: relative;
  background: #FFF;
  padding: 20px;
  width: auto;
  max-width: 500px;
  margin: 20px auto;
}
        </style>
            <h1>WordPress Conversion Booster Settings</h1>
            <?php
            if (isset($_GET["tab"])) {
                $active_tab = $_GET["tab"];
            } else {
                $active_tab = "general";
            } ?>
            Enter your settings below:
            
            <h2 class="nav-tab-wrapper">
                <a href="?page=wcb-options-admin&tab=general" class="nav-tab <?= ($active_tab == 'general') ? 'nav-tab-active' : '' ?>">General</a>
                <a href="?page=wcb-options-admin&tab=mobile" class="nav-tab <?= ($active_tab=='mobile') ? 'nav-tab-active' : ' ' ?>">Mobile</a>
                <!--<a href="?page=wcb-options-admin&tab=woocommerce" class="nav-tab <?= ($active_tab=='woocommerce') ? 'nav-tab-active' : ' ' ?>">Woocommerce</a>-->
                <a href="?page=wcb-options-admin&tab=advanced" class="nav-tab <?= ($active_tab=='advanced') ? 'nav-tab-active' : ' ' ?>">Customize</a> 
                <a href="?page=wcb-options-admin&tab=activation" class="nav-tab <?= ($active_tab=='activation') ? 'nav-tab-active' : ' ' ?>">Upgrade To Pro</a>

            </h2>
            <form action="options.php" method="post">
            <?php
            $positions = array(
                                    "Top Right" => "toast-top-right",
                                    "Top Left" => "toast-top-left",
                                    "Top Full Width" => "toast-top-full-width",
                                    "Top Center" => "toast-top-center",
                                    "Bottom Right" => "toast-bottom-right",
                                    "Bottom Left" => "toast-bottom-left",
                                    "Bottom Full Width" => "toast-bottom-full-width",
                                    "Bottom Center" => "toast-bottom-center"
                                    );
            if ($active_tab == "general") {
                settings_fields("ct_options_general");
                do_settings_sections("ct_options_general");
                
                $enabled = get_option("ct_enabled", "no");
                
                $timeout = get_option("ct_notifications_timeout", 10);
                $notification_position = get_option("ct_tracker_notifications_position", "toast-bottom-right"); ?>
                    
                            <table class="form-table">
                                <tr valign="top">
                                    <th scope="row">Enable Notifications</th>
                                    <td>
                    <input type="checkbox" name="ct_enabled" id="ct_enabled" value="yes" <?= ($enabled=="yes") ? "checked" : " "; ?>>
                                    </td>
                                </tr>
                        <tr valign="top">
                            <th scope="row">Position</th>
                            <td>
                                <select id="" name="ct_tracker_notifications_position">
                <?php

                echo "Notification Position is" .$notification_position;
                foreach ($positions as $key => $value) {
                    $selected_pos = ($value == $notification_position) ? "selected" : " ";
                    echo '<option value="'.$value.'" '.$selected_pos. ' >'.$key. '</option>';
                } ?>
                            </select>
                        <td>
                    </tr>
                    <tr valign="top">
                        <th scope="row">Notifications Timeout (seconds)</th>
                        <td>
                            <select name="ct_notifications_timeout">
                                <?php
                                for ($i=1; $i < 16; $i++) {
                                    if ($i == $timeout) {
                                        echo("<option value='$i' selected>$i</option>");
                                    } else {
                                        echo("<option value='$i'>$i</option>");
                                    }
                                } ?>
                                </select>
                            </td>
                        </tr>
                            </table>
                            <script defer>
                                jQuery(document).ready(function ($) {
                                    $(".popup").magnificPopup({
                                    items: {
                                        src: $("<div class='white-popup'>%time% for time<br />%city% for city<br /> %country% for country<br /><br /><b>Example:</b> <i>\"Someone from %city% %country% made an enquiry %time%\"</i> gives <br /> Someone from London, United Kingdom made an enquiry 5 hours ago </div>")
                                       
                                    }

                                    })
                                });
                            </script>
                            <?php submit_button();
            } elseif ($active_tab == "mobile") {
                settings_fields("ct_options_mobile");
                do_settings_sections("ct_options_mobile");
                $mobile_enabled = get_option("ct_mobile_enable", "no");
                $notification_position_mobile = get_option("ct_mobile_position", "toast-top-center"); ?>
                        <table class="form-table">
                            <tr valign="top">
                                <th scope="row">Enable Mobile Notifications</th>
                                <td>
                                        <input type="checkbox" name="ct_mobile_enable" id="ct_mobile_enable" value="yes" <?= ($mobile_enabled == "yes") ? "checked" : "" ?>>
                                </td>
                            </tr>
                            <tr>
                            <th scope="row">Position</th>
                                <td>
                                    <select name="ct_mobile_position">
        <?php
        foreach ($positions as $key => $value) {
                $selected_pos = ($value == $notification_position_mobile) ? "selected" : " ";
                echo '<option value="'.$value.'" '.$selected_pos. ' >'.$key. '</option>';
        } ?>
                                </select>
                            </td>
                        </tr>
                    </table>
                <?php
                submit_button();
            } elseif ($active_tab == "woocommerce") {
                $chkWoocommerce = get_option("ct_chk_woocommerce", "");
                $wc_message = get_option("ct_wc_tracker_message", "");
                settings_fields("ct_options_woocommerce");
                do_settings_sections("ct_options_woocommerce"); ?>
                    
                        <table class="form-table">
                            <tr valign="top">
                                <th scope="row">Track Woocommerce?</th>
                                <td>
                                        <input type="checkbox" name="ct_chk_woocommerce" id="check_woocommerce" onClick="toggle_WC_Msg(this)" value="checked" <?php echo $chkWoocommerce ?>>
                                </td>
                            </tr>
                            <tr valign="top">
                                <th scope="row">Woocommerce Order Tracking Message</th>
                                <td>
                                        <input type="text" name="ct_wc_tracker_message" <?php if (empty($chkWoocommerce)) {
                                            echo "disabled";
} ?> id="wc_message" size="50" value="<?php echo $wc_message ?>"> <br />
                                <small>Same options as tracking message, but with %name% and %productname%!</small>
                            </td>
                         </tr>
                    </table>
                    <script>
                        jQuery(document).ready(function($){
                            $("#check_woocommerce").on("change", function() {
                                if($(this).is(":checked")){
                                    $("#wc_message").removeAttr("disabled");
                                } else {
                                    $("#wc_message").attr("disabled", "disabled"); 
                                }
                            })
                        })
                    </script>
                    <?php
                    submit_button();
            } elseif ($active_tab=="advanced") {
                $tracker_days = get_option("ct_tracker_days", 4);
            
                settings_fields("ct_options_advanced");
                do_settings_sections("ct_options_advanced"); ?>
                        <table class="form-table">
                            <tr valign="top">
                                <th scope="row">Log Days To Show</th>
                                <td>
                                        <input type="number" name="ct_tracker_days" id="trackerdays" size="50" value="<?php echo $tracker_days ?>">
                                </td>
                            </tr>

                            
                            <tr valign="top">
                                <th scope="row">Custom Funnel(s)</th>
                                <td>
                                        <?php wp_dropdown_pages(array('class' => 'hidden', "id"=> "custom_messages_posts")); ?>
                                        
                                    <div ng-app="CustomFunnel" ng-controller="MsgsCtrl" ng-init="initDropdownData()">
                                    <div class="panel-group">
                                    <input type="hidden" name="ct_custom_funnel" value="{{selections}}" >
                                        <div ng-repeat="key in selections.item track by $index" class="panel panel-default">
                                            <div class="panel-heading">Funnel {{$index+1}}</div>
                                            <div class="panel-body">
                                            <form class="form-inline">
                                                <fieldset>
                                                    <div class="form-group">
                                                        <label>Goal Page</label>
                                                        <select name="" id="" class="form-control" ng-options="item.id as item.value for item in dropDownJson" ng-change="clickDropDown(selections.item[$index]['goal'], '{{selections.item[$index]['goal']}}')" ng-model="selections.item[$index]['goal']"></select>
                                                        
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Message Pages</label>
                                                        <select chosen class="form-control" name="" id="" ng-options="item.id as item.value disable when item.selected==true for item in dropDownJson" ng-model="selections.item[$index]['message_pages']" width="'100%'" multiple ></select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="">Message:</label>
                                                        <input type="text" class="form-control" ng-model="selections.item[$index]['message']" value="" />
                                                        <small><button type="button" class="btn btn-link customize-message">Click here to see message formats</button> </small>
                                                    </div>
                                                    <div class="form-group">
                                                    <button class="btn btn-danger" ng-show="$last" ng-click="removeFunnel($event)" ng-disabled="$first">Remove Funnel</button>
                                                    </div>
                                                    
                                                </fieldset>

                                            </form>
                                            </div>
                                        </div>
                                        <?php if (get_option("ct_activation_status", false) != false) : ?>
                                        <div class="form-group">
                                        <br />
                                            <button class="btn btn-primary" ng-disabled="!selections.item[selections.item.length-1]['message'] ||!selections.item[selections.item.length-1]['goal'] || selections.item[selections.item.length-1]['message_pages'].length == 0" ng-click="addFunnels($event)">Add More Funnels</button>
                                        </div>
                                        <?php endif; ?>
                                        <!--<div class="panel-info">
                                            <div class="panel-heading">Debug Data</div>
                                            <div class="panel-body">
                                                length of selections.item array is{{selections.item.length}}<br/>
                                                        {{dropDownJson}}<br />
                                                        {{selections}}
                                            </div>           
                                        </div>-->
                                        
                                    </div>
                                        
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <script type="text/javascript">
                            $ = jQuery;
                            var app = angular.module('CustomFunnel', ['localytics.directives']);
                            app.controller('MsgsCtrl', function ($scope) { 
                                $scope.dropDownJson = [];         
                                //Selections model with default populated data. In next iteration, its going to be from the input field
                                $scope.selections = <?php $funnel_data =  get_option('ct_custom_funnel', '');
                                if ($funnel_data != '') {
                                    echo $funnel_data;
                                } else {
                                    echo '{item: [{"goal":"","message_pages":[], "message": ""}]}';
                                } ?>
                                
                                $scope.initDropdownData = function () { 
                                    
                                    jQuery(document).ready(function ($) {
                                         $(".customize-message").magnificPopup({
                                            items: {
                                            src: $("<div class='white-popup'>%time% for time<br />%city% for city<br /> %country% for country<br /><br /><b>Example:</b> <i>\"Someone from %city% %country% made an enquiry %time%\"</i> gives <br /> Someone from London, United Kingdom made an enquiry 5 hours ago </div>"),
                                        type: 'inline' } });
                                        $("#custom_messages_posts option").each(function(){
                                        
                                                $scope.dropDownJson.push({"id" : $(this).val(), "value": $(this).text(), "selected" : false});
                                           
                                        }).promise().done(function () 
                                        { 
                                            $scope.$apply();  
                                            return $scope.dropDownJson; 
                                        });
                                    });
                                 }
                                 $scope.clickDropDown = function (newVal, oldVal) {
                                    console.log(newVal);
                                    console.log(oldVal);
                                    //Change selected attribute of new value to true
                                    $scope.dropDownJson.find(function (currentVal, index, array) { 
                                        if(newVal == currentVal.id){
                                            $scope.dropDownJson[index].selected = true;
                                        }
                                     }, newVal);
                                    //Change selected attribute of old value to false
                                    if (oldVal != "") {
                                        $scope.dropDownJson.find(function (currentVal, index, array) {
                                            if (oldVal == currentVal.id) {
                                               $scope.dropDownJson[index].selected = false;
                                            }
                                          }, oldVal);
                                    }
                                  }

                                  $scope.checkSelected = function (value) {
                                      
                                        return value.selected == true;
                                   }
                                   $scope.addFunnels = function (event) {
                                       event.preventDefault();
                                       $scope.selections.item.push({"goal":"","message_pages":[], "message": ""})
                                       console.log("I am adding more funnels");
                                    }
                                    $scope.removeFunnel = function (event) { 
                                        event.preventDefault();
                                        var lastItem = $scope.selections.item.length-1;
                                        $scope.selections.item.splice(lastItem);
                                    }
                                    $scope.loadPopup = function (event) {
                                        event.preventDefault(); 
                                       
                                     }
                             
                            });
                        </script>
                            <?php
                            submit_button();
            } elseif ($active_tab == "activation") {
                settings_fields("ct_options_activation");
                do_settings_sections("ct_options_activation");
                $key = get_option("ct_activation_key", "");
                $status = get_option("ct_activation_status", false) ?>
            <table class="form-table">
                <?php
                if (isset($_SESSION["activated"])) {
                    if ($_SESSION["activated"] == "yes") {
                        echo("<div class='notice notice-success'>Plugin License Activated</div>");
                        unset($_SESSION["activated"]);
                    } elseif ($_SESSION["activated"] == "no") {
                        echo("<div class='notice notice-error'>Invalid Key!</div>");
                        unset($_SESSION["activated"]);
                    }
                } ?>
        
            <tr valign="top">
                <th scope="row">Activation Key</th>
                <td>
                    <input type="text" name="ct_activation_key" id="ct_activation_key" size="50" value="<?=$key?>" <?= (!$status) ? '': 'disabled' ?>><br /><p>
                    <?= (!$status) ? 'To remove watermark, get your activation key <a href="http://wpconversionbooster.com/pro" target="_blank">here</a>' : 'Your plugin license is valid!' ?>
                    </p>
                    
                </td>
            </tr>
            <tr valign="top">
                <th scope="row">Version</th>
                <td><p>
                <?= $status != false ? "<h4>Premium</h4>" : "<h4>Free!</h4>When you upgrade to Pro, you:<ol><li>Get Free Plugin Updates To Keep Up To Date With Security</li><li>Can install the plugin on 3 Sites Licence</li><li>Free Installation</li></ol>"; ?>
                </p>
                </td>
            </tr>
            </table>
        <?php submit_button("Activate") ?>
            </form>      
            </div>
            <?php
            }
    }
}
if (is_admin()) {
    new Combined_Tracker_Class();
}
